gmem kernel module
===================

+-------------+
| Compilation |
+-------------+

You can compile the kernel module 
and the testing application by running

$ make

Kernel version used:
3.8.0-30-generic 64-bit

+---------------------------+
| Loading the kernel module |
+---------------------------+

The make command generation the kernel 
module gmem.ko which has to be loaded
in kernel 

$ sudo insmod gmem.ko

This will create a device file via udev
which will have permissions as 600 owned
by root and group as root.

As per default udev rules, any character 
device created cannot be read or written
using regular UNIX user which is a security
feature.

+---------------------------+
| Reading using gmem_tester |
+---------------------------+

To get the content of the kernel module's
character driver's buffer, run

$ sudo ./gmem_tester show
Hello world! This is Manish and this machine has worked for 240154 seconds


+---------------------------+
| Writing using gmem_tester |
+---------------------------+

To write the contents to kernel module. Run

$ sudo ./gmem_tester write "How are you"
Going to write: How are you
Written 11 bytes

+------------------+
| gmem_tester help |
+------------------+

If you run gmem_tester without any arguments,
it will show you how to run it

$ sudo ./gmem_tester
No operation specifed
Usage: 
	gmem_tester show
	gmem_tester write "Some data"

-------------------------

If you use write but don't specify the data

$ sudo ./gmem_tester write
You need to specify the message
Usage: 
	gmem_tester show
	gmem_tester write "Some data"

--------------------------

If you are running without root, you 
wont be able to read or write because as
a regular user, you can't read or write to 
the character device /dev/gmem

$ ./gmem_tester show
Can not open device file.


+---------------+
| Miscellaneous |
+---------------+

By default when the file is created, it's
permission is

$ ll /dev/gmem 
crw------- 1 root root 250, 0 Sep 19 21:36 /dev/gmem

If you want the created character device file to have 
different permissions, you need to add a rule to udev

Read /etc/udev/rules.d/README for more information
