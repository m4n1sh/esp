#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/pci.h>
#include <linux/spinlock.h>
#include <linux/string.h>
#include <linux/jiffies.h>
#include <linux/vmalloc.h>


#define DEVICE_NAME                 "gmem"
#define BUFFER_SIZE		    256

/* per device structure */
struct gmem_dev {
	struct cdev cdev;               /* The cdev structure */
	char name[20];                  /* Name of device*/
	char in_string[BUFFER_SIZE];	/* buffer for the input string */
	int write_ptr;
} *my_devp;

static dev_t gmem_dev_number;      /* Allotted device number */
struct class *gmem_dev_class;          /* Tie with the device model */


/*
* Open My driver
*/
int gmem_open(struct inode *inode, struct file *file)
{
	struct gmem_dev *my_devp;
	
	printk("\nopening\n");

	/* Get the per-device structure that contains this cdev */
	my_devp = container_of(inode->i_cdev, struct gmem_dev, cdev);

	/* Easy access to cmos_devp from rest of the entry points */
	file->private_data = my_devp;

	return 0;
}

/*
 * Release My driver
 */
int gmem_release(struct inode *inode, struct file *file)
{
	struct gmem_dev *my_devp = file->private_data;
	
	printk("\n%s is closing\n", my_devp->name);
	
	return 0;
}


static ssize_t 
gmem_write(struct file *file, const char *buf,
           size_t count, loff_t *ppos)
{
	int res, buf_left;
	char *write_buffer;

	struct gmem_dev *my_devp = file->private_data;
	write_buffer = (char*)vmalloc(strlen(buf) + 2);
	
	res = copy_from_user(write_buffer, buf, count);	
	
	/* We use the variable buf_left to track the characters which are yet
	 * to be copied or processed.
	 * Every time we copy a character from our buffer to the kernel buffer,
	 * we increment the count of buf_left till we have looked into all of them
	 */
	buf_left = 0;
	while(buf_left < strlen(write_buffer))
	{
		/* We make sure that when our current kernel buffer becomes full
		 * We need to wrap it around, so that we can start from beginning
		 * and keep looping until our input buffer is processed
		 */
		if(my_devp->write_ptr == BUFFER_SIZE - 1)
			my_devp->write_ptr = 0;
		my_devp->in_string[my_devp->write_ptr] = write_buffer[buf_left];
		my_devp->write_ptr++;
		buf_left++;
	}
	
	printk("Writing: %s", write_buffer);
	
	/* After we have written the input buffer to kernel buffer, we
	 * put a null character only if the kernel buffer is not full.
	 * Once the kernel buffer is full, the null character is already at 
	 * the end of the file and we don't need to put it again. Putting it
	 * again might lead to truncation of the kernel buffer if the null
	 * character ends up in middle of the buffer
	 */
	if(strlen(my_devp->in_string) != BUFFER_SIZE-1)
		my_devp->in_string[my_devp->write_ptr] = '\0';

	printk("\n%s \n", my_devp->in_string);

	return count;
}

static ssize_t 
gmem_read(struct file *file, char *buf,
	size_t count, loff_t *ppos)
{
	int res;
	struct gmem_dev *my_devp = file->private_data;
	res = copy_to_user(buf, &my_devp->in_string, BUFFER_SIZE);

	return BUFFER_SIZE - res;
}


/* File operations structure. Defined in linux/fs.h */
static struct file_operations gmem_fops = {
    .owner = THIS_MODULE,           /* Owner */
    .open = gmem_open,              /* Open method */
    .release = gmem_release,        /* Release method */
    .read = gmem_read,		/* Read method */
    .write = gmem_write,            /* Write method */
};

/*
 * Driver Initialization
 */
int __init gmem_driver_init(void)
{
	int ret;

	/* Request dynamic allocation of a device major number */
	if (alloc_chrdev_region(&gmem_dev_number, 0, 1, DEVICE_NAME) < 0) {
			printk(KERN_DEBUG "Can't register device\n"); return -1;
	}

	/* Populate sysfs entries */
	gmem_dev_class = class_create(THIS_MODULE, DEVICE_NAME);

	
	/* Allocate memory for the per-device structure */
	my_devp = kmalloc(sizeof(struct gmem_dev), GFP_KERNEL);
		
	if (!my_devp) {
		printk("Bad Kmalloc\n"); return -ENOMEM;
	}

	/* Request I/O region */
	sprintf(my_devp->name, DEVICE_NAME);


	/* Connect the file operations with the cdev */
	cdev_init(&my_devp->cdev, &gmem_fops);
	my_devp->cdev.owner = THIS_MODULE;

	/* Connect the major/minor number to the cdev */
	ret = cdev_add(&my_devp->cdev, (gmem_dev_number), 1);

	if (ret) {
		printk("Bad cdev\n");
		return ret;
	}

	/* Send uevents to udev, so it'll create /dev nodes */
	device_create(gmem_dev_class, NULL, MKDEV(MAJOR(gmem_dev_number), 0), NULL, DEVICE_NAME);		
	
	sprintf(my_devp->in_string, 
		"Hello world! This is Manish and this machine has worked for %d seconds",
			jiffies_to_msecs(jiffies)/1000);
	my_devp->write_ptr = strlen(my_devp->in_string);

	printk("gmem Driver Initialized.\n");
	return 0;
}
/* Driver Exit */
void __exit gmem_driver_exit(void)
{
	
	/* Release the major number */
	unregister_chrdev_region((gmem_dev_number), 1);

	/* Destroy device */
	device_destroy (gmem_dev_class, MKDEV(MAJOR(gmem_dev_number), 0));
	cdev_del(&my_devp->cdev);
	kfree(my_devp);
	
	/* Destroy driver_class */
	class_destroy(gmem_dev_class);

	printk("gmem Driver removed.\n");
}

module_init(gmem_driver_init);
module_exit(gmem_driver_exit);
MODULE_LICENSE("GPL v2");
