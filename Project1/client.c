/* Demo Application for Lab 1 */
#include <stdio.h>
#include <fcntl.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE 257

void show_usage_exit()
{
	printf("Usage: \n\tgmem_tester show\n\tgmem_tester write \"Some data\"\n");
	exit(1);
}

int main(int argc, char *argv[])
{
	if(argc < 2)
	{
		printf("No operation specifed\n");
		show_usage_exit();
	}

	char *operation = argv[1];
	FILE* fd;
	int res;
	size_t read_res;
	char *buffer = (char*)malloc(BUFFER_SIZE);
	
	if(!strcasecmp(operation, "show"))
	{
		// Read
		fd = fopen("/dev/gmem", "rwb+");
		if (!fd)
		{
			printf("Can not open device file.\n");		
			return 1;
		}

		read_res = fread(buffer, 1, BUFFER_SIZE, fd);
		printf("%s\n", buffer);
		fclose(fd);
		return 0;
	}

	else if(!strcasecmp(operation, "write"))
	{
		if(argc < 3)
		{
			printf("You need to specify the message\n");
			show_usage_exit();
		}
		// Write
		fd = fopen("/dev/gmem", "wb+");
		if (!fd)
		{
			printf("Can not open device file.\n");		
			return 1;
		}

		char *write_buffer = argv[2];
		printf("Going to write: %s\n", write_buffer);

		res = fwrite(write_buffer, 1, strlen(write_buffer), fd);
		printf("Written %d bytes\n", res);

		fclose(fd);
		return 0;
	}

	else
	{
		printf("No such operation: %s\n", operation);
		show_usage_exit();
	}
}
