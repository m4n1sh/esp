#ifndef I2C_FLASH_H
#define I2C_FLASH_H

#define DEVICE_NAME 	"i2c_flash"

#define MC_PAGE_SIZE 	0x40
#define TRAINER_ADDRESS 0x52
#define I2CDEVICE_NO 	2
#define TOTAL_PAGES 	512

#endif
