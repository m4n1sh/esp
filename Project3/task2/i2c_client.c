#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <linux/i2c-dev.h>

#include "i2c_flash.h"

#define I2CDEVICE_NO 2
#define TRAINER_ADDRESS 0x52

#define START_ADDRESS 0
#define ADDRESS_RANGE 0x7fff

#define TOTAL_PAGES 512

#define SLEEP_PERIOD 500000

int page_cur, fd;

char* get_device_file()
{
	char* filename = (char*)malloc(20);
	sprintf(filename, "/dev/i2c_flash");
	return filename;
}

void update_cursor(int count, bool from_start)
{
	if(from_start)
		page_cur = 0;
	page_cur = (page_cur + count) % TOTAL_PAGES;
}

void print_page(char* buf, int num_pages)
{
	printf("%s\n", buf);
}


int seek_EEPROM(int offset)
{
    printf("Seeking to position: %d\n", offset);
    lseek(fd, offset, SEEK_SET);
    update_cursor(offset, true);
	
	return 0;
}

int main(int argc, char *argv[])
{
	int i,j, num_pages;
	char *buf, *read_buf;
	
	page_cur = 0;
	
	if(argc != 2)
	{
		printf("Usage:\n\teeprom_client stringtowrite\n");
		return 1;	
	}
	
	buf = argv[1];
	printf("%s\n", buf);
	num_pages = ceil(( (float)strlen(buf) )/MC_PAGE_SIZE);
	//printf("String length: %d | Page Size: %d | Pages Number: %d\n", strlen(buf), PAGE_SIZE, num_pages);
	
	fd = open(get_device_file(), O_RDWR);
	if (!fd)
	{
		printf("Can not open device file.\n");		
		return 1;
	}
	
	lseek(fd, 0, SEEK_SET);
    update_cursor(0, true);
	printf("Current page cursor position: %d\n", page_cur);
	printf("-------------------------------------------------------------------------\n");

	// Try to write the contents
	write(fd, buf, num_pages);
	update_cursor(num_pages-1, false);
	printf("After writing %d pages, current page cursor position: %d\n", num_pages, page_cur);
	printf("-------------------------------------------------------------------------\n");
	
	// Go back to the location 
	// where the data has been written
	if(lseek(fd, page_cur - (num_pages-1), SEEK_SET) < 0)
		return 1;
	update_cursor(page_cur - (num_pages-1) - 1, false);
	printf("Current page cursor position: %d\n", page_cur);
	printf("-------------------------------------------------------------------------\n");
	
	read_buf = (char*)calloc(sizeof(char) * MC_PAGE_SIZE * num_pages, sizeof(char));
	read(fd, (char*)read_buf, num_pages);
	update_cursor(num_pages, false);
	usleep(SLEEP_PERIOD);
	print_page(read_buf, num_pages);
	
	close(fd);
	return 0;
}
