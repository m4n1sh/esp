/*
    i2c-dev.c - i2c-bus driver, char device interface

    Copyright (C) 1995-97 Simon G. Vogl
    Copyright (C) 1998-99 Frodo Looijaard <frodol@dds.nl>
    Copyright (C) 2003 Greg Kroah-Hartman <greg@kroah.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    MA 02110-1301 USA.
*/

/* Note that this is a complete rewrite of Simon Vogl's i2c-dev module.
   But I have used so much of his original code and ideas that it seems
   only fair to recognize him as co-author -- Frodo */

/* The I2C_RDWR ioctl code is written by Kolja Waschk <waschk@telos.de> */

#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/notifier.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/list.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <linux/jiffies.h>
#include <linux/uaccess.h>

#include "i2c_flash.h"

/*
 * An i2c_dev represents an i2c_adapter ... an I2C or SMBus master, not a
 * slave (i2c_client) with which messages will be exchanged.  It's coupled
 * with a character special file which is accessed by user mode drivers.
 *
 * The list of i2c_dev structures is parallel to the i2c_adapter lists
 * maintained by the driver model, and is updated using bus notifications.
 */
struct i2c_dev {
	struct list_head list;
	struct i2c_adapter *adap;
	struct device *dev;
};

#define I2C_MINORS	256
static LIST_HEAD(i2c_dev_list);
static DEFINE_SPINLOCK(i2c_dev_list_lock);
static int i2cf_major;
static int page_cur;

static struct i2c_dev *i2c_dev_get_by_minor(unsigned index)
{
	struct i2c_dev *i2c_dev;

	spin_lock(&i2c_dev_list_lock);
	list_for_each_entry(i2c_dev, &i2c_dev_list, list) {
		if (i2c_dev->adap->nr == index)
			goto found;
	}
	i2c_dev = NULL;
found:
	spin_unlock(&i2c_dev_list_lock);
	return i2c_dev;
}

static struct i2c_dev *get_free_i2c_dev(struct i2c_adapter *adap)
{
	struct i2c_dev *i2c_dev;

	if (adap->nr >= I2C_MINORS) {
		printk(KERN_ERR "i2cf-dev: Out of device minors (%d)\n",
		       adap->nr);
		return ERR_PTR(-ENODEV);
	}

	i2c_dev = kzalloc(sizeof(*i2c_dev), GFP_KERNEL);
	if (!i2c_dev)
		return ERR_PTR(-ENOMEM);
	i2c_dev->adap = adap;

	spin_lock(&i2c_dev_list_lock);
	list_add_tail(&i2c_dev->list, &i2c_dev_list);
	spin_unlock(&i2c_dev_list_lock);
	return i2c_dev;
}

static void return_i2c_dev(struct i2c_dev *i2c_dev)
{
	spin_lock(&i2c_dev_list_lock);
	list_del(&i2c_dev->list);
	spin_unlock(&i2c_dev_list_lock);
	kfree(i2c_dev);
}

static ssize_t show_adapter_name(struct device *dev,
				 struct device_attribute *attr, char *buf)
{
	struct i2c_dev *i2c_dev = i2c_dev_get_by_minor(MINOR(dev->devt));

	if (!i2c_dev)
		return -ENODEV;
	return sprintf(buf, "%s\n", i2c_dev->adap->name);
}
static DEVICE_ATTR(name, S_IRUGO, show_adapter_name, NULL);

/* ------------------------------------------------------------------------- */

/*
 * After opening an instance of this character special file, a file
 * descriptor starts out associated only with an i2c_adapter (and bus).
 *
 * Using the I2C_RDWR ioctl(), you can then *immediately* issue i2c_msg
 * traffic to any devices on the bus used by that adapter.  That's because
 * the i2c_msg vectors embed all the addressing information they need, and
 * are submitted directly to an i2c_adapter.  However, SMBus-only adapters
 * don't support that interface.
 *
 * To use read()/write() system calls on that file descriptor, or to use
 * SMBus interfaces (and work with SMBus-only hosts!), you must first issue
 * an I2C_SLAVE (or I2C_SLAVE_FORCE) ioctl.  That configures an anonymous
 * (never registered) i2c_client so it holds the addressing information
 * needed by those system calls and by this SMBus interface.
 */
 
static void update_cursor(int count, bool from_start)
{
	if(from_start)
		page_cur = 0;
	page_cur = (page_cur + count) % TOTAL_PAGES;
}

static loff_t 
i2cdev_llseek (struct file *file, loff_t offset, int whence)
{
	if(whence == SEEK_SET)
	{
		int ret;
		unsigned short mem_loc;
		unsigned char buf[2];
		struct i2c_client *client;
		printk(KERN_ERR "Offset set to %d\n", (int)offset);
		client = file->private_data;
		
		page_cur = (int)offset;
		mem_loc = offset * MC_PAGE_SIZE;
		buf[1] = mem_loc & 0xff;
		buf[0] = (mem_loc>>8) & 0xff;
		
		ret = i2c_master_send(client, buf, 2);
	}
	return 0;
}

static ssize_t 
i2cdev_read(struct file *file, char __user *buf, size_t count, loff_t *offset)
{
	char *tmp;
	int ret;

	struct i2c_client *client = file->private_data;

	count *= MC_PAGE_SIZE;

	tmp = kmalloc(count, GFP_KERNEL);
	if (tmp == NULL)
		return -ENOMEM;

	pr_debug("i2cf-dev: i2cf-%d reading %zu bytes.\n",
		iminor(file->f_path.dentry->d_inode), count);

	ret = i2c_master_recv(client, tmp, count);
	if (ret >= 0)
		ret = copy_to_user(buf, tmp, count) ? -EFAULT : ret;
	printk(KERN_ERR "Fetched string: %s\n", tmp);
	kfree(tmp);
	return ret;
}

static ssize_t 
i2cdev_write(struct file *file, const char __user *buf,	size_t count, loff_t *offset)
{
	int i, ret;
	char *tmp;
	int write_bytes, bytes_total, buf_pages, pages_to_copy, bytes_left;
	struct i2c_client *client = file->private_data;
	tmp = kmalloc(count*MC_PAGE_SIZE, GFP_KERNEL);
	copy_from_user(tmp, buf, count*MC_PAGE_SIZE);

	write_bytes = MC_PAGE_SIZE+2;
	bytes_total = strlen((char*)buf);
	buf_pages = bytes_total/MC_PAGE_SIZE;
	// If bytes_total is not a multiple of MC_PAGE_SIZE
	if(buf_pages*MC_PAGE_SIZE < bytes_total)
		buf_pages++;	
	
	pages_to_copy = min(buf_pages, (int)count);
	bytes_left = bytes_total;
	
	printk(KERN_ERR "Total pages: %d\nGoing to write %s\n", (int)pages_to_copy, tmp);
	
	for(i=0;i<pages_to_copy;i++)
	{
		unsigned short mem_loc =  (page_cur + i) * MC_PAGE_SIZE;
		char *write_buf = (char*)kcalloc(write_bytes, sizeof(char), GFP_KERNEL);
		
		write_buf[1] = mem_loc & 0xff;
		write_buf[0] = (mem_loc>>8) & 0xff;
		
		memcpy(write_buf+2, buf + i*MC_PAGE_SIZE, bytes_left <= MC_PAGE_SIZE? bytes_left: MC_PAGE_SIZE);
		bytes_left = bytes_left <= MC_PAGE_SIZE ? bytes_left : bytes_left - MC_PAGE_SIZE;
		
		printk(KERN_ERR "Going to write page %s", buf + i*MC_PAGE_SIZE);
		i2c_master_send(client, write_buf, MC_PAGE_SIZE);
	}
	update_cursor(pages_to_copy-1, false);

	return count;
}

static int i2cdev_open(struct inode *inode, struct file *file)
{
	unsigned int minor = iminor(inode);
	struct i2c_client *client;
	struct i2c_adapter *adap;
	struct i2c_dev *i2c_dev;

	i2c_dev = i2c_dev_get_by_minor(minor);
	if (!i2c_dev)
		return -ENODEV;

	adap = i2c_get_adapter(i2c_dev->adap->nr);
	if (!adap)
		return -ENODEV;

	/* This creates an anonymous i2c_client, which may later be
	 * pointed to some address using I2C_SLAVE or I2C_SLAVE_FORCE.
	 *
	 * This client is ** NEVER REGISTERED ** with the driver model
	 * or I2C core code!!  It just holds private copies of addressing
	 * information and maybe a PEC flag.
	 */
	client = kzalloc(sizeof(*client), GFP_KERNEL);
	if (!client) {
		i2c_put_adapter(adap);
		return -ENOMEM;
	}
	snprintf(client->name, I2C_NAME_SIZE, "i2c_flash");

	client->adapter = adap;
	client->addr = TRAINER_ADDRESS;
	file->private_data = client;

	return 0;
}

static int i2cdev_release(struct inode *inode, struct file *file)
{
	struct i2c_client *client = file->private_data;

	i2c_put_adapter(client->adapter);
	kfree(client);
	file->private_data = NULL;

	return 0;
}

static const struct file_operations i2cdev_fops = {
	.owner		= THIS_MODULE,
	.llseek		= i2cdev_llseek,
	.read		= i2cdev_read,
	.write		= i2cdev_write,
	.open		= i2cdev_open,
	.release	= i2cdev_release,
};

/* ------------------------------------------------------------------------- */

static struct class *i2c_dev_class;

static int i2cdev_attach_adapter(struct device *dev, void *dummy)
{
	
	struct i2c_adapter *adap;
	struct i2c_dev *i2c_dev;
	int res;

	if (dev->type != &i2c_adapter_type)
		return 0;
	adap = to_i2c_adapter(dev);
	if(adap->nr == I2CDEVICE_NO)
	{

		i2c_dev = get_free_i2c_dev(adap);
		if (IS_ERR(i2c_dev))
			return PTR_ERR(i2c_dev);

		/* register this i2c device with the driver core */
		i2c_dev->dev = device_create(i2c_dev_class, &adap->dev,
				     	MKDEV(i2cf_major, adap->nr), NULL,
				     	DEVICE_NAME);
		if (IS_ERR(i2c_dev->dev)) {
			res = PTR_ERR(i2c_dev->dev);
			goto error;
		}
		res = device_create_file(i2c_dev->dev, &dev_attr_name);
		if (res)
			goto error_destroy;

		pr_debug("i2c_flash: adapter [%s] registered as minor %d\n",
		 	adap->name, adap->nr);
		return 0;
error_destroy:
		device_destroy(i2c_dev_class, MKDEV(i2cf_major, adap->nr));
error:
		return_i2c_dev(i2c_dev);
		return res;
		}

	return 0;
}

static int i2cdev_detach_adapter(struct device *dev, void *dummy)
{
	struct i2c_adapter *adap;
	struct i2c_dev *i2c_dev;

	if (dev->type != &i2c_adapter_type)
		return 0;
	adap = to_i2c_adapter(dev);

	if(adap->nr == I2CDEVICE_NO)
	{
		i2c_dev = i2c_dev_get_by_minor(adap->nr);
		if (!i2c_dev) /* attach_adapter must have failed */
			return 0;

		device_remove_file(i2c_dev->dev, &dev_attr_name);
		return_i2c_dev(i2c_dev);
		device_destroy(i2c_dev_class, MKDEV(i2cf_major, adap->nr));

		pr_debug("i2c_flash_dev: adapter [%s] unregistered\n", adap->name);
	}
	return 0;
}

static int i2cdev_notifier_call(struct notifier_block *nb, unsigned long action,
			 void *data)
{
	struct device *dev = data;

	switch (action) {
	case BUS_NOTIFY_ADD_DEVICE:
		return i2cdev_attach_adapter(dev, NULL);
	case BUS_NOTIFY_DEL_DEVICE:
		return i2cdev_detach_adapter(dev, NULL);
	}

	return 0;
}

static struct notifier_block i2cdev_notifier = {
	.notifier_call = i2cdev_notifier_call,
};

/* ------------------------------------------------------------------------- */

/*
 * module load/unload record keeping
 */

static int __init i2c_flash_dev_init(void)
{
	int res;

	printk(KERN_INFO "i2c /dev entries driver\n");

	i2cf_major = register_chrdev(0, "i2c_flash", &i2cdev_fops);
	if (i2cf_major <=0 )
		goto out;

	i2c_dev_class = class_create(THIS_MODULE, "i2c_flash");
	if (IS_ERR(i2c_dev_class)) {
		goto out_unreg_chrdev;
	}

	/* Keep track of adapters which will be added or removed later */
	res = bus_register_notifier(&i2c_bus_type, &i2cdev_notifier);
	if (res)
		goto out_unreg_class;

	/* Bind to already existing adapters right away */
	i2c_for_each_dev(NULL, i2cdev_attach_adapter);

	return 0;

out_unreg_class:
	class_destroy(i2c_dev_class);
out_unreg_chrdev:
	unregister_chrdev(i2cf_major, "i2c_flash");
out:
	printk(KERN_ERR "%s: Driver Initialisation failed\n", __FILE__);
	return res;
}

static void __exit i2c_flash_dev_exit(void)
{
	bus_unregister_notifier(&i2c_bus_type, &i2cdev_notifier);
	i2c_for_each_dev(NULL, i2cdev_detach_adapter);
	class_destroy(i2c_dev_class);
	unregister_chrdev(i2cf_major, "i2c_flash");
}

MODULE_AUTHOR("Frodo Looijaard <frodol@dds.nl> and "
		"Simon G. Vogl <simon@tk.uni-linz.ac.at> and "
		"Manish Sinha <manishsinha@ubuntu.com>");
MODULE_DESCRIPTION("I2C Microchip EEPROM 24AA256/24LC256/24FC256 driver");
MODULE_LICENSE("GPL");

module_init(i2c_flash_dev_init);
module_exit(i2c_flash_dev_exit);
