#ifndef EEPROM_IO_H
#define EEPROM_IO_H

char* get_device_file();
void update_cursor(int count, bool from_start);
void print_page(char* buf, int num_pages);

int write_EEPROM(const void *buf, int count);
int read_EEPROM(void *buf, int count);
int seek_EEPROM(int offset);

#endif
