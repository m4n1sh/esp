#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <linux/i2c-dev.h>

#define I2CDEVICE_NO 2
#define TRAINER_ADDRESS 0x52
#define PAGE_SIZE 0x40

#define START_ADDRESS 0
#define ADDRESS_RANGE 0x7fff

#define TOTAL_PAGES 512

#define SLEEP_PERIOD 500000

int page_cur, fd;

char* get_device_file()
{
	char* filename = (char*)malloc(20);
	sprintf(filename, "/dev/i2c-%d", I2CDEVICE_NO);
	return filename;
}

void update_cursor(int count, bool from_start)
{
	if(from_start)
		page_cur = 0;
	page_cur = (page_cur + count) % TOTAL_PAGES;
}

void print_page(char* buf, int num_pages)
{
	printf("%s", buf);
}

int write_EEPROM(const void *buf, int count)
{
	int i, bytes_written, memcpy_bytes;
	int write_bytes = PAGE_SIZE+2;
	int bytes_left = strlen((char*)buf);
	
	for(i=0;i<count;i++)
	{
		unsigned short mem_loc =  (page_cur + i) * PAGE_SIZE;
		char *write_buf = (char*)calloc(write_bytes, sizeof(char));
		write_buf[1] = mem_loc & 0xff;
		write_buf[0] = (mem_loc>>8) & 0xff;
		printf("Memory location to write: %2x%2x\n", write_buf[0], write_buf[1]);
	
		if(bytes_left < PAGE_SIZE)
			memcpy_bytes = bytes_left;
		else
		{
			memcpy_bytes = PAGE_SIZE;
			memcpy_bytes -= PAGE_SIZE;
		}
		memcpy(write_buf+2, buf + i*PAGE_SIZE, bytes_left <= PAGE_SIZE? bytes_left: PAGE_SIZE);
		bytes_left = bytes_left <= PAGE_SIZE ? bytes_left : bytes_left - PAGE_SIZE;
		
		bytes_written = write(fd, write_buf, PAGE_SIZE);
	
		usleep(SLEEP_PERIOD);
		if(bytes_written != PAGE_SIZE) {
        	printf("I2C write failed | Errno: %d | Error: %s\n", errno, strerror(errno));
        	return -1;
    	}
    }
    update_cursor(count-1, false);
    
	return 0;
}

int read_EEPROM(void *buf, int count)
{	
	read(fd, (char*)buf, count*PAGE_SIZE);
	update_cursor(count, false);
	usleep(SLEEP_PERIOD);
	return 0;
}

int seek_EEPROM(int offset)
{
	int bytes_written;
	// Figure out the memory location based on the
	// EEPROM page requested
	unsigned short mem_loc = offset * PAGE_SIZE;
	printf("Seek Page: %d | Base address: 0x%2x\n", offset, mem_loc);
	
	// We don't need the payload and only need the
	// memory address. So we just need to put the 
	// memory address in upper and lower memory byte
	unsigned char buf[2];
	buf[1] = mem_loc & 0xff;
	buf[0] = (mem_loc>>8) & 0xff;
	bytes_written = write(fd, buf, 2);
	usleep(SLEEP_PERIOD);
	if(bytes_written != 2) {
        printf("I2C seek failed | Errno: %d | Error: %s\n", errno, strerror(errno));
        return -1;
    }
    update_cursor(offset, true);
	
	return 0;
}

int main(int argc, char *argv[])
{
	int i,j, num_pages;
	char *buf, *read_buf;
	
	page_cur = 0;
	
	if(argc != 2)
	{
		printf("Usage:\n\teeprom_client stringtowrite\n");
		return 1;	
	}
	
	buf = argv[1];
	printf("%s\n", buf);
	num_pages = ceil(( (float)strlen(buf) )/PAGE_SIZE);
	//printf("String length: %d | Page Size: %d | Pages Number: %d\n", strlen(buf), PAGE_SIZE, num_pages);
	
	fd = open(get_device_file(), O_RDWR);
	if (!fd)
	{
		printf("Can not open device file.\n");		
		return 1;
	}
	
	if (ioctl(fd, I2C_SLAVE, TRAINER_ADDRESS) == -1)
	{
		printf("Failed to initialize the address of the Trainer board\n");
		return 1;
	}
	
	seek_EEPROM(0);
	printf("Current page cursor position: %d\n", page_cur);
	printf("-------------------------------------------------------------------------\n");

	// Try to write the contents
	write_EEPROM(buf, num_pages);
	printf("After writing %d pages, current page cursor position: %d\n", num_pages, page_cur);
	printf("-------------------------------------------------------------------------\n");
	
	// Go back to the location 
	// where the data has been written
	if(seek_EEPROM(page_cur - (num_pages-1)) < 0)
		return 1;
	printf("Current page cursor position: %d\n", page_cur);
	printf("-------------------------------------------------------------------------\n");
	
	read_buf = (char*)calloc(sizeof(char) * PAGE_SIZE * num_pages, sizeof(char));
	read_EEPROM(read_buf, num_pages);
	print_page(read_buf, num_pages);
	
	close(fd);
	return 0;
}
