#ifndef I2C_FLASH_H
#define I2C_FLASH_H

#define DEVICE_NAME 	"i2c_flash"

#define MC_PAGE_SIZE 	0x40
#define TRAINER_ADDRESS 0x52
#define I2CDEVICE_NO 	2
#define TOTAL_PAGES 	512

#define	WRITE_FINISHED	0
#define WRITE_SUBMITTED	2
#define WRITE_BUSY		1
#define WRITE_FAILED	3

#define	READ_FINISHED	0
#define READ_SUBMITTED	2
#define READ_BUSY		1

#endif
