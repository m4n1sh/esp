#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <signal.h>
#include <time.h>
#include <limits.h>
#include <string.h>

struct node
{
	long long value;
	struct node *next;
};

struct node* head;
static jmp_buf exception_env;
long int total_fibs;

void alarm_handler (int signum)
{
	printf("Executing longjmp\n");
    longjmp(exception_env, 1);          
}

struct node* add_node(struct node *tail, long long value)
{
	struct node *new_tail;
	new_tail = (struct node*)malloc(sizeof(struct node));
	
	new_tail->value = value;
	new_tail->next = NULL;
	
	tail->next = new_tail;
	
	return new_tail;
}

void fibo()
{
	int i; long long diff;
	struct node *first, *second;
	
	head = first = (struct node*)malloc(sizeof(struct node));
	second = (struct node*)malloc(sizeof(struct node));
	first->value = 0;
	second->value = 1;
	first->next = second;
	second->next = NULL;
	
	while(1)
	{
		diff = LONG_MAX - second->value;
		add_node(second, (diff - first->value) > 0 ? first->value+second->value: first->value - diff);
		total_fibs++;
		
		first = first->next;
		second = second->next;
	}
	
}

void print_nodes(struct node* head)
{
	while(head != NULL)
	{
		printf("%lld\t", head->value);
		head = head->next;
	}
}

int main(int argc, char *argv[])
{
	time_t start_time, end_time;
	signal (SIGALRM, alarm_handler);
	
	total_fibs = 0;
	start_time = time(NULL);
	printf("Setting alarm to 1 sec\n");
	alarm (1);
	
	if (setjmp(exception_env)) {
		end_time = time(NULL);
		if(argc > 1 && !strcmp("--verbose", argv[1]))
			print_nodes(head);
		else
			printf("Total Fiboniacci calculated: %ld. To show them all run with --verbose\n", total_fibs);
		printf("\nNumber of second elapsed: %ld\n", (long)end_time - (long)start_time);
		
		
	}
	else {
		printf("Starting Fibo\n");
		fibo();
	}
	
	
	return 0;
}
