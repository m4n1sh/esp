#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>
#include <memory.h>

#define TOTAL_THREADS	10

struct thread_args_data
{
	int value;
} thread_data[TOTAL_THREADS];

pthread_t threads[TOTAL_THREADS];
struct sigaction act;
int first_signal;

void* worker_thread(void* arg)
{
	int sel_ret, sig, i;
	fd_set thd;
	sigset_t set;
	
	struct thread_args_data *data = (struct thread_args_data*)arg;
	printf("Thread%d started\n", data->value);

	sigemptyset(&set);
	sigaddset(&set, SIGIO);
	sigwait(&set, &sig);
	if(sig == SIGIO)
	{
		printf("Thread%d caught SIGIO\n", data->value);
		if(first_signal < 0)
		{
			first_signal = data->value;
			printf("Notifying other threads about SIGIO\n");
			for(i=0;i<TOTAL_THREADS;i++)
				if(i != data->value)
					pthread_kill(threads[i], SIGIO);
		}
	}
}

int main(int argc, char *argv[])
{
	int i;
	sigset_t set;
	
	printf("The processid is %d\n", getpid());
	first_signal = -1;
	
	sigaddset(&set, SIGIO);
	sigprocmask(SIG_BLOCK, &set, NULL);
	
	for(i=0;i<TOTAL_THREADS;i++)
	{
		thread_data[i].value = i;
		pthread_create(&threads[i], NULL, worker_thread, &thread_data[i]);
	}
		
	for(i=0;i<TOTAL_THREADS;i++)
	{
		pthread_join(threads[i], NULL);
	}
		
	return 0;
}
