#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdbool.h>
#include <pthread.h>
#include <signal.h>
#include <linux/input.h>
#include <sys/select.h>

#include "imprecise_computation.h"

#define MOUSE "/dev/input/event8"
#define DOUBLE_CLICK_DURATION 300000
#define SECS_TO_MICROSECS 1000000

struct double_click
{
	bool valid_first_click;
	struct timeval first_click;
} click_data;

bool is_double_click(struct timeval first_click, struct timeval second_click)
{
	long difference;
	difference = (second_click.tv_sec - first_click.tv_sec) * SECS_TO_MICROSECS
					+ (second_click.tv_usec - first_click.tv_usec);
	return (difference < DOUBLE_CLICK_DURATION)? true: false;
}

void* run_mouse_double_click()
{
	int fd, sel_ret;
	fd_set mouse;
	ssize_t read_ret;
	struct input_event mice_data;
	
	click_data.valid_first_click = false;
					
	fd = open(MOUSE, O_RDONLY);
	if(fd < 0)
	{
		printf("Unable to open mice: %s\n", MOUSE);
		exit(1);
	}
	
	FD_ZERO(&mouse);
	FD_SET(fd, &mouse);
	
	while(1)
	{
		sel_ret = select(fd+1, &mouse, NULL, NULL, NULL);
		if(sel_ret > 0)
		{
			read_ret = read(fd, &mice_data, sizeof(struct input_event));
			if(mice_data.type == EV_KEY && mice_data.code == BTN_RIGHT && mice_data.value == 0x1)
			{
				printf("\nSecond: %ld | MicroSecond: %ld \n", mice_data.time.tv_sec, mice_data.time.tv_usec);
				if(!click_data.valid_first_click)
				{
					click_data.valid_first_click = true;
					click_data.first_click = mice_data.time;
				}
				else
				{
					if(is_double_click(click_data.first_click, mice_data.time))
					{
						printf("Double Click\n");
						raise(SIGTERM);
						click_data.valid_first_click = false;
					}
					else
						click_data.first_click = mice_data.time;
				}
			}
		}
	}
	
	close(fd);
}

void* run_imprecise(void *arg)
{
	signal (SIGTERM, sigterm_handler);
	start_time = time(NULL);
	if (setjmp(exception_env)) {
		end_time = time(NULL);
		printf("Total Fiboniacci calculated: %ld. To show them all run with --verbose\n", total_fibs);
		printf("\nNumber of second elapsed: %ld\n", (long)end_time - (long)start_time);		
	}
	else {
		printf("Starting Fibo\n");
		fibo();
	}
}

int main(int argc, char *argv[])
{
	pthread_t mouse, computation;
	pthread_create(&mouse, NULL, &run_mouse_double_click, NULL);
	pthread_create(&computation, NULL, &run_imprecise, NULL);
	
	pthread_join(mouse, NULL);
	pthread_join(computation, NULL);
	
	return 0;
}
