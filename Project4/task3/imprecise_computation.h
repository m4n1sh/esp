#ifndef IMPRECISE_H
#define IMPRECISE_H

#include <setjmp.h>
#include <time.h>

struct node
{
	long long value;
	struct node *next;
};

void fibo();
void sigterm_handler (int signum);
struct node* add_node(struct node *tail, long long value);

struct node* head;
static jmp_buf exception_env;
long int total_fibs;
time_t start_time, end_time;

#endif
