#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <signal.h>
#include <time.h>
#include <limits.h>
#include <string.h>

#include "imprecise_computation.h"

void sigterm_handler (int signum)
{
	if(signum == SIGTERM)
	{
		printf("\nReceived SIGTERM. Executing longjmp\n");
    	longjmp(exception_env, 1);          
    }
}

struct node* add_node(struct node *tail, long long value)
{
	struct node *new_tail;
	new_tail = (struct node*)malloc(sizeof(struct node));
	
	new_tail->value = value;
	new_tail->next = NULL;
	
	tail->next = new_tail;
	usleep(100);
	
	return new_tail;
}

void fibo()
{
	int i; long long diff;
	struct node *first, *second;
	
	head = first = (struct node*)malloc(sizeof(struct node));
	second = (struct node*)malloc(sizeof(struct node));
	first->value = 0;
	second->value = 1;
	first->next = second;
	second->next = NULL;
	
	while(1)
	{
		diff = LONG_MAX - second->value;
		add_node(second, (diff - first->value) > 0 ? first->value+second->value: first->value - diff);
		total_fibs++;
		printf("#");
		
		first = first->next;
		second = second->next;
	}
	
}

