from hashlib import sha384
from datetime import datetime
from random import random

fd = open("data", "w")
for x in range(220):
    r = int((random() * 100)  % 80)
    print("{0}\n".format(r))
    data = sha384(datetime.now().__str__().encode('utf-8')).hexdigest()
    fd.write("\"{0}\", ".format(data[0:r]))

fd.close()
