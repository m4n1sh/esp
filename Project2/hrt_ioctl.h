#ifndef HRT_IOCTL_H
#define HRT_IOCTL_H
#include <linux/ioctl.h>
 
typedef struct
{
    int value;
} hrt_arg_t;

#define HRT_MAGIC 'h'
 
#define START_HRT _IO(HRT_MAGIC, 1)
#define STOP_HRT _IO(HRT_MAGIC, 2)
#define SET_HRT _IOW(HRT_MAGIC, 3, hrt_arg_t *)
 
#endif
