/* Demo Application for Lab 1 */
#include <stdio.h>
#include <fcntl.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#include "utils.h"

#define BUFFER_SIZE 150
#define NUM_MSGS 100

int flag1=0, flag2=0;


void read_queue(char *dev)
{
	FILE* fd, *fdt;
	int res;
	size_t read_res;
	char *buffer = (char*)malloc(BUFFER_SIZE);
	char *time_buf = (char*)malloc(100);
	
	fdt = fopen("/dev/hrt", "r");
	fread(time_buf, 1, 10, fdt);
	fclose(fdt);

	fd = fopen(dev, "rwb+");
	if (!fd)
	{
		printf("Can not open device file.\n");
		return;
	}

	read_res = fread(buffer, 1, BUFFER_SIZE, fd);
	if(strlen(buffer))
	{
		printf("Device: %s | Read: %s | Write, Enqueue, Dequeue, Token, Content: %s\n", dev, time_buf, buffer);
	}
	else
		printf("Nothing read\n");
	fclose(fd);
}

void read_data_alt1()
{
	char *devname1 = (char*)malloc(100);
	sprintf(devname1, "/dev/squeue1");
	read_queue(devname1);
}

void read_data_alt2()
{
	char *devname2 = (char*)malloc(100);
	sprintf(devname2, "/dev/squeue2");
	read_queue(devname2);
}

void read_data_alt()
{
	int i;
	while(1)
	{
		read_data_alt1();
		usleep(rand()%10);
		read_data_alt2();
		usleep(rand()%10);
		// Signals that reader and writer have finished
		if(flag1 && flag2)
			break;
	}
}

void write_data(char *devname, int token_start, int data_start)
{
	FILE *fd, *fdt;
	int res,i;
	size_t read_res;
	char *buffer = (char*)malloc(BUFFER_SIZE);
	char *time_buf = (char*)malloc(100);
	char *temp, *broken;

	for(i=0;i<100;i++)
	{
		// Get the time
		fdt = fopen("/dev/hrt", "r");
		fread(time_buf, 1, 10, fdt);
		fclose(fdt);
	
		fd = fopen(devname, "wb+");
		if (!fd)
		{
			printf("Can not open device file.\n");		
			return;
		}

		char *content = random_str[data_start+i];
		char *data = (char*)malloc(BUFFER_SIZE);
		sprintf(data, "%s,%d,%s", time_buf, token_start+i, content);

		printf("Device: %s | Write time: %s | Token: %d | Content: %s\n", devname, time_buf, token_start+i, content);
		res = fwrite(data, 1, strlen(data), fd);
		fclose(fd);
		usleep(rand()%10);
	}
}

void write_data_queue1()
{
	char *devname = (char*)malloc(100);
	sprintf(devname, "/dev/squeue1");
	write_data(devname, 1000, 0);
	flag1=1;
}

void write_data_queue2()
{
	char *devname = (char*)malloc(100);
	sprintf(devname, "/dev/squeue2");
	write_data(devname, 2000, 70);
	flag2=1;
}

int main(int argc, char *argv[])
{
	int i;
	pthread_t read_thread, queue1_thread[3], queue2_thread[3];
	
	for(i=0;i<3;i++)
	{
		pthread_create(&queue1_thread[i], NULL, (void*) &write_data_queue1, NULL);
		pthread_create(&queue2_thread[i], NULL, (void*) &write_data_queue2, NULL);
	}
	pthread_create(&read_thread, NULL, (void*) &read_data_alt, NULL);

	for(i=0;i<3;i++)
	{
		pthread_join(queue1_thread[i], NULL);
		pthread_join(queue2_thread[i], NULL);
	}
	pthread_join(read_thread, NULL);

	return 0;
}
