#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/pci.h>
#include <linux/spinlock.h>
#include <linux/string.h>
#include <linux/jiffies.h>
#include <linux/vmalloc.h>
#include <linux/random.h>
#include <linux/mutex.h>


#define DEVICE_CLASS                 "squeue"
#define DEVICE_NAME1                 "squeue1"
#define DEVICE_NAME2                 "squeue2"
#define BUFFER_SIZE		    256
#define QUEUE_SIZE			10

extern long get_hrt();

/* per device structure */
struct squeue_dev {
	struct cdev cdev;               /* The cdev structure */
	char name[20];                  /* Name of device*/
	struct mutex mut;				/* Mutex for reading/writing for this device queue */
} *my_devp1, *my_devp2;

static dev_t squeue_dev_number;      /* Allotted device number */
struct class *squeue_dev_class;          /* Tie with the device model */


/* token queue */
struct squeue_circle {
	int token;
	long write_time;
	long enqueue_time;
	long dequeue_time;
	long read_time;
	char content[81];
};

struct squeue_bound {
	int front;
	int back;
	int count;
};

struct squeue_circle queue1[QUEUE_SIZE], queue2[QUEUE_SIZE];
struct squeue_bound *bound1, *bound2;

/*
* Open My driver
*/
int squeue_open(struct inode *inode, struct file *file)
{
	struct squeue_dev *my_devp;

	/* Get the per-device structure that contains this cdev */
	my_devp = container_of(inode->i_cdev, struct squeue_dev, cdev);

	/* Easy access to cmos_devp from rest of the entry points */
	file->private_data = my_devp;

	printk("\nopening squeue: %s\n",  my_devp->name);

	return 0;
}

/*
 * Release My driver
 */
int squeue_release(struct inode *inode, struct file *file)
{
	struct squeue_dev *my_devp = file->private_data;
	
	printk("\n%s is closing\n", my_devp->name);
	
	return 0;
}


static struct squeue_circle*
get_queue(char *devname)
{
	return strcmp(devname, DEVICE_NAME1)? queue2 : queue1;
}

static struct squeue_bound*
get_queue_bound(char *devname)
{
	return strcmp(devname, DEVICE_NAME1)? bound2 : bound1;
}

static void
print_queue_contents(struct squeue_circle* queue)
{
	int i;
	struct squeue_circle node;
	for(i=0;i<QUEUE_SIZE;i++)
	{
		node = queue[i];
		printk("Token: %d | Content: %s | Write: %lu\n", node.token, node.content, node.write_time);
	}
}

static int
is_queue_empty(struct squeue_bound *bound)
{
	printk("Back: %d, Front: %d\n", bound->back, bound->front);
	return (bound->count < QUEUE_SIZE) && (bound->back == bound->front);
}

static int
is_queue_full(struct squeue_bound *bound)
{
	return bound->count == QUEUE_SIZE;
}


static void 
enqueue(struct squeue_circle* queue, struct squeue_bound *bound, int token, int timestamp, char *content)
{
	struct squeue_circle node;
	node = queue[bound->front];

	strcpy(node.content, content);
	node.token = token;
	node.write_time = timestamp;
	node.enqueue_time = get_hrt();
	queue[bound->front] = node;

	bound->front = (bound->front + 1) % QUEUE_SIZE;
	bound->count++;
	print_queue_contents(queue);
}

static int 
dequeue(struct squeue_circle* queue, struct squeue_bound *bound, char *content)
{
	struct squeue_circle node;
	node = queue[bound->back];
	node.dequeue_time = get_hrt();
	queue[bound->back] = node;

	sprintf(content, "%d,%d,%d,%d,%s", node.write_time, node.enqueue_time, node.dequeue_time, node.token, node.content);

	bound->back = (bound->back + 1) % QUEUE_SIZE;
	bound->count--;
	return strlen(content)+1;
	print_queue_contents(queue);
}


static ssize_t 
squeue_write(struct file *file, const char *buf,
           size_t count, loff_t *ppos)
{
	int res, token_len, content_len;
	char *write_buffer, *token, *content, *timestamp;
	long tokenid, timestampid;

	struct squeue_dev *my_devp;
	struct squeue_circle *queue;
	struct squeue_bound *bound;

	tokenid = 10;

	my_devp = file->private_data;
	// Get the correct queue based on the device name
	queue = get_queue(&my_devp->name);
	bound = get_queue_bound(&my_devp->name);

	if (is_queue_full(bound))
	{
		printk("Queue is full\n");
		return 0;
	}

	write_buffer = (char*)vmalloc(strlen(buf) + 2);
	res = copy_from_user(write_buffer, buf, count);
	printk("Write: %s\n", write_buffer);

	timestamp = (char*)kmalloc(100, GFP_KERNEL);
	token = (char*)kmalloc(100, GFP_KERNEL);

	token = strchr(write_buffer, ',');
	strncpy(timestamp, write_buffer, strlen(write_buffer) - strlen(token));
	timestamp[strlen(write_buffer) - strlen(token)] = '\0';
	token = token+1;
	kstrtol(timestamp, 10, &timestampid);
	
	content = (char*)kmalloc(100, GFP_KERNEL);
	content = strchr(token, ',');
	content_len = strlen(content);
	strncpy(token, token, strlen(token) - content_len);
	token[strlen(token) - content_len] = '\0';
	content = content+1;
	kstrtol(token, 10, &tokenid);

	//printk("Timestamp: %s| Content: %s | Len: %d | Token: %s | TLength: %lu\n", timestamp, content,content_len, token, tokenid);

	mutex_lock_killable(&my_devp->mut);
	enqueue(queue, bound, (int)tokenid, (int)timestampid, content);
	mutex_unlock(&my_devp->mut);
	return count;
}

static ssize_t 
squeue_read(struct file *file, char *buf,
	size_t count, loff_t *ppos)
{
	int res;
	char *write_buffer;

	struct squeue_dev *my_devp;
	struct squeue_circle *queue;
	struct squeue_bound *bound;
	
	
	// Get the correct queue based on the device name

	my_devp = file->private_data;
	queue = get_queue(&my_devp->name);
	bound = get_queue_bound(&my_devp->name);

	write_buffer = (char*)vmalloc(100);
	
	if (!is_queue_empty(bound))
	{
		mutex_lock_killable(&my_devp->mut);
		res = dequeue(queue, bound, write_buffer);
		mutex_unlock(&my_devp->mut);
	}
	else
	{
		printk("Queue is empty\n");
		strcpy(write_buffer, "");
	}

	copy_to_user(buf, write_buffer, strlen(write_buffer)+1);

	printk("Read called: %s\n", write_buffer);

	return BUFFER_SIZE;
}


/* File operations structure. Defined in linux/fs.h */
static struct file_operations squeue_fops = {
    .owner = THIS_MODULE,           /* Owner */
    .open = squeue_open,              /* Open method */
    .release = squeue_release,        /* Release method */
    .read = squeue_read,		/* Read method */
    .write = squeue_write,            /* Write method */
};

/*
 * Driver Initialization
 */
int __init squeue_driver_init(void)
{
	int ret, err;
	dev_t devno1, devno2;
	struct device *device;

	/* Request dynamic allocation of a device major number */
	if (alloc_chrdev_region(&squeue_dev_number, 0, 2, DEVICE_CLASS) < 0) {
			printk(KERN_DEBUG "Can't register device\n"); return -1;
	}

	/* Populate sysfs entries */
	squeue_dev_class = class_create(THIS_MODULE, DEVICE_CLASS);

	
	/* Allocate memory for two device structure */
	my_devp1 = kmalloc(sizeof(struct squeue_dev), GFP_KERNEL);
	my_devp2 = kmalloc(sizeof(struct squeue_dev), GFP_KERNEL);
		
	if (!my_devp1) {
		printk("Bad Kmalloc\n"); return -ENOMEM;
	}
	if (!my_devp2) {
		printk("Bad Kmalloc\n"); return -ENOMEM;
	}

	/* Request I/O region */
	sprintf(my_devp1->name, DEVICE_NAME1);
	sprintf(my_devp2->name, DEVICE_NAME2);


	/* Connect the file operations with the cdev */
	cdev_init(&my_devp1->cdev, &squeue_fops);
	cdev_init(&my_devp2->cdev, &squeue_fops);
	my_devp1->cdev.owner = THIS_MODULE;
	my_devp2->cdev.owner = THIS_MODULE;

	mutex_init(&my_devp1->mut);
	mutex_init(&my_devp2->mut);

	devno1 = MKDEV(MAJOR(squeue_dev_number), 0);
	devno2 = MKDEV(MAJOR(squeue_dev_number), 1);

	/* Connect the major/minor number to the cdev */
	ret = cdev_add(&my_devp1->cdev, devno1, 1);
	if (ret) {
		printk("Bad cdev for dev1\n");return ret;
	}
	ret = cdev_add(&my_devp2->cdev, devno2, 1);
	if (ret) {
		printk("Bad cdev for dev2\n");return ret;
	}

	/* Send uevents to udev, so it'll create /dev nodes */
	device = device_create(squeue_dev_class, NULL, devno1, NULL, DEVICE_NAME1);	
	if (IS_ERR(device)) {
		err = PTR_ERR(device);
		printk(KERN_WARNING "[target1] Error %d while trying to create %s\n",err, DEVICE_NAME1);
	}
	device = device_create(squeue_dev_class, NULL, 	devno2, NULL, DEVICE_NAME2);	
	if (IS_ERR(device)) {
		err = PTR_ERR(device);
		printk(KERN_WARNING "[target2] Error %d while trying to create %s\n",err, DEVICE_NAME2);
	}
	
	printk("squeue Driver Initialized.\n");

	bound1 = (struct squeue_bound*)kmalloc(sizeof(struct squeue_bound), GFP_KERNEL);
	bound1->front = 0;bound1->back = 0;bound1->count = 0;
	bound2 = (struct squeue_bound*)kmalloc(sizeof(struct squeue_bound), GFP_KERNEL);	
	bound2->front = 0;bound2->back = 0;bound2->count = 0;

	return 0;
}
/* Driver Exit */
void __exit squeue_driver_exit(void)
{
	
	/* Release the major number */
	unregister_chrdev_region((squeue_dev_number), 1);

	/* Destroy device */
	device_destroy (squeue_dev_class, MKDEV(MAJOR(squeue_dev_number), 0));
	device_destroy (squeue_dev_class, MKDEV(MAJOR(squeue_dev_number), 1));

	cdev_del(&my_devp1->cdev);
	cdev_del(&my_devp2->cdev);
	
	kfree(my_devp1);
	kfree(my_devp2);
	
	/* Destroy driver_class */
	class_destroy(squeue_dev_class);

	printk("squeue Driver removed.\n");
}

module_init(squeue_driver_init);
module_exit(squeue_driver_exit);
MODULE_LICENSE("GPL v2");
