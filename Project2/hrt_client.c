/* Demo Application for Lab 1 */
#include <stdio.h>
#include <fcntl.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "hrt_ioctl.h"

#define BUFFER_SIZE 20

int main(int argc, char *argv[])
{
	int fd;
	int res;
	size_t read_res;
	char *buffer = (char*)malloc(BUFFER_SIZE);
	
	// Read
	fd = open("/dev/hrt", O_RDWR);
	/*if (!fd)
	{
		printf("Can not open device file.\n");		
		return 1;
	}

	read_res = fread(buffer, 1, BUFFER_SIZE, fd);
	printf("Size Read: %zu, Content: %s\n", read_res, buffer);*/
	
	if (ioctl(fd, START_HRT) == -1)
	{
       		perror("query_apps ioctl clr\n");
   	}
	if (ioctl(fd, STOP_HRT) == -1)
	{
       		perror("query_apps ioctl clr\n");
   	}

	hrt_arg_t t;
	t.value = 23;
	if (ioctl(fd, SET_HRT, &t) == -1)
	{
       		perror("query_apps ioctl clr\n");
   	}
    
	close(fd);
	return 0;
}
