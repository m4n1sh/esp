#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <linux/pci.h>
#include <linux/spinlock.h>
#include <linux/string.h>
#include <linux/vmalloc.h>
#include <linux/init.h>			
#include <linux/clk.h>		
#include <linux/irq.h>
#include <linux/interrupt.h>

#include <asm/uaccess.h>
#include <asm/io.h>			
#include <plat/dmtimer.h>	

#include <linux/timer.h>
#include <linux/time.h>

#include "hrt_ioctl.h"



#define DEVICE_NAME         "hrt"
#define BUFFER_SIZE		    256

/* per device structure */
struct hrt_dev {
	struct cdev cdev;               /* The cdev structure */
	char name[20];   
} *my_devp;

static dev_t hrt_dev_number;      /* Allotted device number */
struct class *hrt_dev_class;          /* Tie with the device model */

static struct omap_dm_timer *dtimer;

void hrt_prepare()
{
	dtimer = omap_dm_timer_request();
	if(dtimer == NULL){
		printk("Could not acquire dmtimer\n");

		return;
	}
	omap_dm_timer_set_source(dtimer, OMAP_TIMER_SRC_SYS_CLK);
	omap_dm_timer_set_prescaler(dtimer, 0);
	omap_dm_timer_set_load_start(dtimer, 0, 0);
}

void hrt_destroy()
{
	// Stop and Release the timer
	omap_dm_timer_stop(dtimer);
	omap_dm_timer_free(dtimer);
}

/*
* Open My driver
*/
int hrt_open(struct inode *inode, struct file *file)
{
	struct hrt_dev *my_devp;
	
	printk("\nOpening HRT");

	/* Get the per-device structure that contains this cdev */
	my_devp = container_of(inode->i_cdev, struct hrt_dev, cdev);

	/* Easy access to cmos_devp from rest of the entry points */
	file->private_data = my_devp;

	return 0;
}

/*
 * Release My driver
 */
int hrt_release(struct inode *inode, struct file *file)
{
	struct hrt_dev *my_devp = file->private_data;
	
	printk("\n%s is closing", my_devp->name);
	
	return 0;
}

long get_hrt()
{
	return omap_dm_timer_read_counter(dtimer);
}
EXPORT_SYMBOL(get_hrt);

static ssize_t 
hrt_read(struct file *file, char *buf,
	size_t count, loff_t *ppos)
{
	int res;
	char time[10];
	

	sprintf(time, "%lu",get_hrt());
	res = copy_to_user(buf, time, 10);
	printk("\ncopy_to_user could not copy %d bytes", res);
	printk("\nTime now: %s, length: %zu", buf, strlen(buf));

	return 10;
}



static long
hrt_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	hrt_arg_t t;
	switch(cmd)
	{
		case START_HRT:
			omap_dm_timer_start(dtimer);
			printk("\nStart Timer\n");
			break;
		case STOP_HRT:
			omap_dm_timer_stop(dtimer);
			printk("\nStop Timer\n");
			break;
		case SET_HRT:
			copy_from_user(&t, (hrt_arg_t*)arg, sizeof(hrt_arg_t));
			omap_dm_timer_write_counter(dtimer, t.value);
			printk("\nSet Timer to %d\n", t.value);
			break;
	}
  	return 0;
}


/* File operations structure. Defined in linux/fs.h */
static struct file_operations hrt_fops = {
    .owner = THIS_MODULE,           /* Owner */
    .open = hrt_open,              /* Open method */
    .release = hrt_release,        /* Release method */
    .read = hrt_read,		/* Read method */
    .unlocked_ioctl = hrt_ioctl,	/* ioctl method */
};

/*
 * Driver Initialization
 */
int __init hrt_driver_init(void)
{
	int ret;

	/* Request dynamic allocation of a device major number */
	if (alloc_chrdev_region(&hrt_dev_number, 0, 1, DEVICE_NAME) < 0) {
			printk(KERN_DEBUG "Can't register device\n"); return -1;
	}

	/* Populate sysfs entries */
	hrt_dev_class = class_create(THIS_MODULE, DEVICE_NAME);

	
	/* Allocate memory for the per-device structure */
	my_devp = kmalloc(sizeof(struct hrt_dev), GFP_KERNEL);
		
	if (!my_devp) {
		printk("Bad Kmalloc\n"); return -ENOMEM;
	}

	/* Request I/O region */
	sprintf(my_devp->name, DEVICE_NAME);


	/* Connect the file operations with the cdev */
	cdev_init(&my_devp->cdev, &hrt_fops);
	my_devp->cdev.owner = THIS_MODULE;

	/* Connect the major/minor number to the cdev */
	ret = cdev_add(&my_devp->cdev, (hrt_dev_number), 1);

	if (ret) {
		printk("Bad cdev\n");
		return ret;
	}

	/* Send uevents to udev, so it'll create /dev nodes */
	device_create(hrt_dev_class, NULL, MKDEV(MAJOR(hrt_dev_number), 0), NULL, DEVICE_NAME);		

	hrt_prepare();

	printk("\nhrt Driver Initialized.");
	return 0;
}
/* Driver Exit */
void __exit hrt_driver_exit(void)
{
	
	/* Release the major number */
	unregister_chrdev_region((hrt_dev_number), 1);

	/* Destroy device */
	device_destroy (hrt_dev_class, MKDEV(MAJOR(hrt_dev_number), 0));
	cdev_del(&my_devp->cdev);
	kfree(my_devp);
	
	/* Destroy driver_class */
	class_destroy(hrt_dev_class);
	hrt_destroy();

	printk("\nhrt Driver removed.");
}

module_init(hrt_driver_init);
module_exit(hrt_driver_exit);
MODULE_LICENSE("GPL v2");
