#include <logd.h>
#include <stdlib.h>
#include <stdio.h>
#include <bbconsole.h>

int is_queue_empty(struct queue_bound *bound)
{
	return (bound->count < QUEUE_SIZE) && (bound->back == bound->front);
}

int is_queue_full(struct queue_bound *bound)
{
	return bound->count == QUEUE_SIZE;
}

void put(struct log_item* queue, struct queue_bound *bound, int log_level, char *content)
{
	int i = 0;
	struct log_item node;
	node = queue[bound->front];
	
	node.log_level = log_level;
	while(*content != '\0')
	{
		node.data[i] = *content;
		serial_putc(*content);
		i++;content++;
	}
	node.data[i] = '\0';
	node.cur = 0;
	
	queue[bound->front] = node;
	bound->front = (bound->front + 1) % QUEUE_SIZE;
}
char get(struct log_item* queue, struct queue_bound *bound, int log_level)
{
	char c;
	struct log_item node;
	node = queue[bound->back];
	while(node.log_level > log_level) 
	{
		bound->back = (bound->back + 1) % QUEUE_SIZE;
		bound->count--;
		
		if(is_queue_empty(bound))
		{
			return 255;
		}
		
		node = queue[bound->back];
	} 
	
	c = node.data[node.cur];

	if (node.data[node.cur] == '\0')
	{
		bound->back = (bound->back + 1) % QUEUE_SIZE;
		bound->count--;
	}
	else
	{
		node.cur++;
		queue[bound->back] = node;
	}
	
	return c;
}

