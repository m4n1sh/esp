#include "include/logd.h"

static void
print_queue_contents(struct log_item* queue)
{
	int i;
	struct log_item node;
	for(i=0;i<QUEUE_SIZE;i++)
	{
		node = queue[i];
		printf("Log Level :%d | Data: %s | Cursor: %d\n", node.log_level, node.data, node.cur);
	}
}

int main()
{
	int i;char c;
	qbound = (struct queue_bound*)malloc(sizeof(struct queue_bound));
	qbound->front = 0;qbound->back = 0;qbound->count = 0;
	put(queue, qbound, LOG_NOTICE, "One");
	put(queue, qbound, LOG_INFO, "Two");
	put(queue, qbound, LOG_NOTICE, "Four");
	put(queue, qbound, LOG_NOTICE, "Manish");
	put(queue, qbound, LOG_ERROR, "Sinha");
	put(queue, qbound, LOG_ERROR, "Hillsboro");
	put(queue, qbound, LOG_ERROR, "Tempe");
	put(queue, qbound, LOG_ERROR, "Seattle");
	put(queue, qbound, LOG_ERROR, "NYC");
	put(queue, qbound, LOG_ERROR, "Portland");
	
	i= 0;
	while(!is_queue_empty(qbound))
	{
		i++;
		c = get(queue, qbound, LOG_NOTICE);
		//if(c >0)
			printf("%d-> %c\n", i, c);
	}

	print_queue_contents(queue);
	
	return 0;
}
