#ifndef __LOGD_H_
#define __LOGD_H_			1

#define	LOG_ALERT	0
#define	LOG_ERROR	1
#define	LOG_WARNING	2
#define	LOG_NOTICE	3
#define	LOG_INFO	4
#define	LOG_DEBUG	5

#define MIN_LEVEL LOG_NOTICE

#define QUEUE_SIZE	10

struct log_item {
	int log_level;
	char data[100];
	int cur;
};

struct queue_bound {
	int front;
	int back;
	int count;
};

struct log_item queue[QUEUE_SIZE];
struct queue_bound *qbound;

int is_queue_empty(struct queue_bound *bound);
int is_queue_full(struct queue_bound *bound);

void put(struct log_item* queue, struct queue_bound *bound, int log_level, char *content);
char get(struct log_item* queue, struct queue_bound *bound, int log_level);

#endif	/* __LOGD_H_ */
